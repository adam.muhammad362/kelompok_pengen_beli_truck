# Cara Install OS LINUX di VM
- Menyiapkan OS Linux (Ubuntu).
- Buka Virtual Box.
- Pilih New untuk membuat Virtual Machine baru.
- Tambahkan Optical Disk Drive baru dan cari OS Linux yang sudah disiapkan sebelumnya.
- Lakukan instalasi Linux Ubuntu.
- Masukkan Nama Host, Domain, dan Kata Sandi Root.
- Tunggu sampai proses instalasi selesai.
- Sistem Operasi Linux Debian sudah berhasil di install.
 
